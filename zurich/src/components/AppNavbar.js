import {useState, useContext} from 'react';

import {Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default function AppNavbar() {

  // to store user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem('email'));

  // const {user} = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#home ">Z</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
{/*            <Nav.Link href="#home ">Home</Nav.Link>
          
           
            <Nav.Link href="#logout">Logout</Nav.Link>*/}
            
            
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            
            
          </Nav>
        </Navbar.Collapse>
        </Container>
    </Navbar>
  );
}