const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName : {
        type : String,
        // Requires the data for this our fields/properties to be included when creating a record.
        // The "true" value defines if the field is required or not and the second element in the array is the message that will be printed out in our terminal when the data is not present
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },
    orders: [{
        products: [{
            productName: {
                type: String,
                required: [true, "Product Name is required"]
            },
            quantity: {
                type: Number,
                required: [true, "Quantity is required!"]
            },
            subtotal: {
            	type: Number,
                required: [true, "Subtotal is required!"]
            }
        }],
        totalAmount: {
            type: Number,
            required: [true, "Total amount is required!"]
        },
        purchasedOn: {
            type: Date,
            default: new Date()
        }
    }]

});




module.exports = mongoose.model("User", userSchema);