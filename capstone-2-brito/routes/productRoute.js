const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


router.get("/active" , (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/:productId" , (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId/update", auth.verify, (req, res) => {
	const data = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});




router.patch("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});



module.exports = router;


