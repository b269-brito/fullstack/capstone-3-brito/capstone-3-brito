const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(
		resultFromController => res.send(resultFromController));
});



router.post("/login", (req,res) =>{
	userController.loginUser(req.body).then(
		resultFromController => res.send(resultFromController));
});


// router.post("/addcart", auth.verify, (req, res) => {
// 	let data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		products: req.body.products
// 	}
// 	userController.userCheckout(data).then(resultFromController => res.send(resultFromController));
// });



router.post("/details", auth.verify, (req,res) =>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId : userData.id}).then(
		resultFromController => res.send(resultFromController));
});


router.patch("/:userId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	userController.userToAdmin(req.params, data).then(resultFromController => res.send(resultFromController));
});

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});



module.exports = router;