const User = require("../models/User");
const Product = require("../models/Product");

const bcrypt = require("bcrypt");

const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		};
	});
};


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>
	{	
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password, result.password);
			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)
			}
			} else {
				return false;
			};
		};
	});
}




// module.exports.userCheckout = async (data) => {
//   const foundUser = await User.findById(data.userId).then(async (user) => {
//     if (user.isAdmin) {
//       let message = Promise.resolve("User must Not be ADMIN to access this!");
//         return message.then((value) => {
//           return {value};
//         });
//     }

//     user.orders = user.orders || [];
//     const products = [];
//     let totalAmount = 0;

    
//     for (let i = 0; i < data.products.length; i++) {
//       const orderedProduct = data.products[i]
    
//       await Product.findById(orderedProduct.productId).then(async(foundProduct)=> { 
 
//         const subtotal = foundProduct.price * orderedProduct.quantity;
      
//         products.push({
//           productName: foundProduct.name,
//           quantity: orderedProduct.quantity,
//           subtotal: subtotal
//         })
      
//         totalAmount += subtotal
//       })
//     };

  
 
//     user.orders.push({
//       products: products,
//       totalAmount: totalAmount,
//       purchasedOn: new Date()
//     });


//     await user.save();
//     return user;
//   });
// }






module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};


module.exports.userToAdmin= (reqParams, data) => {
	if (data.isAdmin){

	let updateAdminField = {
		isAdmin: true
	};
	return User.findByIdAndUpdate(reqParams.userId,updateAdminField).then((user,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};
let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value) => {
		return {value};
	});

};





